/*
 * Copyright (c) 2012-2016 Arne Schwabe
 * Distributed under the GNU GPL v2 with additional terms. For full terms see the file doc/LICENSE.txt
 */

package de.blinkt.openvpn.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.blinkt.openvpn.BuildConfig;
import de.blinkt.openvpn.VpnProfile;

/**
 * Created by arne on 08.01.17.
 */

// Until I find a good solution

public class Preferences {

    static SharedPreferences getSharedPreferencesMulti(String name, Context c) {
        return c.getSharedPreferences(name, Context.MODE_MULTI_PROCESS | Context.MODE_PRIVATE);

    }


    public static SharedPreferences getDefaultSharedPreferences(Context c) {
        return c.getSharedPreferences(c.getPackageName() + "_preferences", Context.MODE_MULTI_PROCESS | Context.MODE_PRIVATE);

    }


    public static void setVpnProfile(SharedPreferences prefs, String key, String value){
        prefs.edit().putString(key, value).commit();
    }
    public static VpnProfile getVpnProfile(SharedPreferences prefs, String key){
        Gson gson = new Gson();
        String data = prefs.getString(key, "");
        writeLogOnSD(data);
        return !data.equalsIgnoreCase("") ? gson.fromJson(data, VpnProfile.class) : null;
    }

    public static VpnProfile parseStrToVpnProfile(String data){
        Gson gson = new Gson();
        return !data.equalsIgnoreCase("") ? gson.fromJson(data, VpnProfile.class) : null;
    }

    public static String getRootFolder(){
        String path = Environment.getExternalStorageDirectory().toString() + "/Android/data/"+ BuildConfig.APPLICATION_ID+"/cache/" ;
        File root = new File(path);
        if (!root.exists()) {
            root.mkdirs();
        }
        return  Environment.getExternalStorageDirectory().toString() + "/Android/data/"+ BuildConfig.APPLICATION_ID+"/cache/" ;
    }

    public static void writeLogOnSD(String sBody) {
        SimpleDateFormat DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat FULLDATETIMEFORMAT = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.ENGLISH);
        try {
            String fileName = DATEFORMAT.format(new Date()).toUpperCase();

            File root = new File(getRootFolder(), "API-LOGS");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, fileName+".txt");
            write(root+"", sBody);
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append(FULLDATETIMEFORMAT.format(new Date()).toUpperCase()
                    + ": " + sBody + "\n\n");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Boolean write(String path, String fcontent){
        try {

            String fpath = path;

            File file = new File(fpath);

            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fcontent);
            bw.close();

            Log.d("Suceess","Sucess");
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }


}
